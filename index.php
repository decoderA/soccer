<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Soccer</title>
		<meta name="description" content="">
		<meta name="viewport" content="height=device-height,
                      width=device-width, initial-scale=1.0,
                      minimum-scale=1.0, maximum-scale=1.0,
                      user-scalable=no, target-densitydpi=device-dpi">

		<link rel="stylesheet" href="./css/main.css?<?= rand(1, 1000000) ?>">
		<link rel="shortcut icon" type="image/png" href="./images/favicon.png"/>
	</head>

	<body>
		<div class="main">
			<div class="main-title">
				<img src="images/gat-logo.svg" alt="">
				<h1>Welcome To The GAT Soft Table Soccer Championship App</h1>
				<h4>The Best App Ever Seen</h4>
				<p>Thanks For Using Our Services.</p>
			</div>

			<div class="reset">
				<button class="persons-remove-button blue clear-radius" onclick="resetGame()">Reset game</button>
			</div>

			<div class="players-group">
				<div class="field">
					<label for="">Soccer Players First Group</label>
					<ul class="persons-wrap">
					</ul>
					<input type="text" class="bordered clear-radius transparent bordered"
					       placeholder="Add persons with space pressing, allowed (-)" id="input-0" onfocusout="addTag(event)"
					       onkeypress="addTag(event)" onkeydown="backspaceRemoveTags(event)"/>
				</div>
				<div class="field">
					<label for="">Soccer Players Second Group</label>
					<ul class="persons-wrap">
					</ul>
					<input type="text" class="bordered clear-radius transparent bordered"
					       placeholder=" Add persons with space pressing, allowed (-)" id="input-1" onfocusout="addTag(event)"
					       onkeypress="addTag(event)" onkeydown="backspaceRemoveTags(event)"/>
				</div>
				<div class="buttons-wrap">
					<button class="persons-save-button blue clear-radius" onclick="saveSoccerPlayers()">Save Soccer Players
					</button>
				</div>

				<div class="overlay">
					<span>Players section disabled after generating groups. If you want change it just press reset button</span>
				</div>
			</div>

			<div class="generate-teams">
				<button class="generate-teams-button blue clear-radius" onclick="generateGroupMatches()">Generate Groups
				</button>

				<div class="group-blocks collapse-wrap" data-wrap="group-wrap">

				</div>
			</div>

			<div class="semifinal-wrap">
				<button class="blue clear-radius generate-semifinal-matches hidden" onclick="generateSemifinal()">Generate
					Semifinal Matches
				</button>

				<div class="semifinal-matches-wrap collapse-wrap" data-wrap="semifinal-wrap">

				</div>
			</div>

			<div class="final-wrap">
				<button class="blue clear-radius generate-final-matches hidden" onclick="generateFinal()">Generate Final
					Matches
				</button>

				<div class="final-matches-wrap collapse-wrap" data-wrap="final-wrap">

				</div>
			</div>

			<div class="show-winners">
				<div class="pedestal hide">
					<img src="images/pedestal.jpg" alt="">

					<div class="pedestal-wrap">

					</div>
				</div>
			</div>
		</div>

		<footer>
		<p>© 2019 Aram. All Rights Reserved</p>
	</footer>

		<script src="js/global.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/functions.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/actions.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/players.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/groups.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/semifinal.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/final.js?<?= rand(1, 1000000) ?>"></script>
		<script src="js/check.js?<?= rand(1, 1000000) ?>"></script>
	</body>
</html>