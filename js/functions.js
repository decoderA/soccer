let playerUrl = './read.php?path=./data/players.json';
let teamsUrl = './read.php?path=./data/group-matches.json';
let semiFinalUrl = './read.php?path=./data/semifinal-matches.json';
let finalUrl = './read.php?path=./data/final-matches.json';

die = () => {
	throw new Error("Something went badly wrong!");
};

idGenerator = () => {
	return Date.now().toString(36) + Math.random().toString(6).substr(2, 8);
};

getStringNumber = (string) => {
	return string.replace(/^\D+/g, '');
};

getNameById = (playersArray, id) => {
	let name;

	playersArray.forEach(playersArray => {
		let filter = playersArray.players.filter(filtered => (filtered.id === id));

		if (filter.length > 0) {
			name = filter[0].name;
		}
	});

	if (name) return name;
};

getRandomNUmber = (max) => {
	return Math.floor(Math.random() * max);
};

getSiteConfigFromLocalStorage = () => {
	let soccerConfig;

	try {
		soccerConfig = JSON.parse(localStorage.getItem('siteConfig'));
	} catch(e) {
		soccerConfig = {};
	}

	return soccerConfig;
};

setSiteConfigToLocalStorage = (params = {}) => {
	let soccerConfig = getSiteConfigFromLocalStorage();

	soccerConfig = {...{}, ...soccerConfig, ...params};

	localStorage.setItem('siteConfig', JSON.stringify(soccerConfig));
};

generateRandomGroups = (playersState) => {
	let arrayLength = playersState[0].players.length;

	let firstIndex, secondIndex, flag = true;

	let groupPlayers = [
		{ players: [] }, { players: [] }
	];

	let groupMatches = [
		{
			groupName: '',
			groupPlayers: [],
			groupId: 0,
			games: [],
			gamesCount: 0
		},
		{
			groupName: '',
			groupPlayers: [],
			groupId: 1,
			games: [],
			gamesCount: 0
		}
	];

	while (flag) {
		firstIndex = getRandomNUmber(arrayLength);
		secondIndex = getRandomNUmber(arrayLength);

		let fPlayer = playersState[0].players[firstIndex];
		let sPlayer = playersState[1].players[secondIndex];


		if (!fPlayer.chosen && !sPlayer.chosen) {
			if(groupPlayers[0].players.length !== arrayLength) {
				groupPlayers[0].players.push(fPlayer, sPlayer);
			} else {
				groupPlayers[1].players.push(fPlayer, sPlayer);
				fPlayer.chosen = true;
				sPlayer.chosen = true;
			}

			fPlayer.chosen = true;
			sPlayer.chosen = true;
		}

		if (groupPlayers[0].players.length === arrayLength && groupPlayers[1].players.length === arrayLength) {
			flag = false;
		}
	}


	groupPlayers.forEach((groupPlayersArray, index) => {
		groupMatches[index].groupName = `${index === 0 ? 'First' : 'Second'} Group`;

		groupPlayersArray.players.forEach(player => {
			groupMatches[index].groupPlayers.push(player.id);

			groupPlayersArray.players.forEach(childPlayer => {
				if (player !== childPlayer) {
					groupMatches[index].games.push({
						firstPlayer: {id: player.id, gf: [0,0], ga: [0,0]},
						secondPlayer: {id: childPlayer.id, gf: [0,0], ga: [0,0]},
						winner: '',
						gameId: 'g' + idGenerator(),
						gamePlayed: false
					})
				}
			});

			groupPlayersArray.players = groupPlayersArray.players.filter(e => e !== player);
		});
	});

	groupMatches.forEach(item => {
		item.gamesCount = item.games.length;
	});

	return groupMatches;
};

getGroupPlayerMatches = (groupMatchesData, id) => {
	let finalScores;

	groupMatchesData.forEach(gamesArray => {
		let filtered = gamesArray.games.filter(filtered => ((filtered.firstPlayer.id === id || filtered.secondPlayer.id === id) && filtered.gamePlayed === true));

		if (filtered.length > 0) {
			finalScores = filtered;
		}
	});

	if (finalScores) {
		finalScores = getGamesResults(finalScores, id);

		return finalScores;
	}
};

getPlayerMatches = (data, id) => {
	let finalScores;

	let filtered = data.filter(filtered => ((filtered.firstPlayer.id === id || filtered.secondPlayer.id === id) && filtered.gamePlayed === true));

	if(filtered && filtered.length > 0) {
		finalScores = filtered;
	}

	if (finalScores) {
		finalScores = getGamesResults(finalScores, id);
		return finalScores;
	}
};

getGamesResults = (filteredArray, id) => {
	let playerScores = new Player();
	playerScores.id = id;
	playerScores.name = getNameById(playersState, id);

	let calculatePlayerScores = (game, player) => {
		playerScores.gf = game[player].gf;
		playerScores.ga = game[player].ga;

		if (game.winner === '0') {
			playerScores.player.tie += 1;
		} else if (game.winner === game[player].id) {
			playerScores.player.win += 1;
		} else {
			playerScores.player.loose += 1;
		}
	};

	filteredArray.forEach(game => {
		if (game.firstPlayer.id === id) {
			calculatePlayerScores(game, 'firstPlayer');
		} else if (game.secondPlayer.id === id) {
			calculatePlayerScores(game, 'secondPlayer');
		}
	});

	return playerScores.player;
};

compareEqualScores = (groupMatchesData, playerOne, playerTwo) => {
	let result;

	let fPlayer = new Player(playerOne);
	let sPlayer = new Player(playerTwo);

	groupMatchesData.forEach((gamesArray, index) => {
		gamesArray.games.filter(filtered => {
			let players = [filtered.firstPlayer.id, filtered.secondPlayer.id];

			if (players.includes(fPlayer.id) && players.includes(sPlayer.id) && filtered.gamePlayed === true) {
				if (filtered.winner === '0') {
					if (fPlayer.gd > sPlayer.gd) {
						result = fPlayer.player;
					} else if (fPlayer.gd < sPlayer.gd) {
						result = sPlayer.player;
					} else if (fPlayer.gd === sPlayer.gd && checkGroupMatchesFinished(groupMatchesData)) {
						groupMatchesData[index].games.push({
							firstPlayer: {id: fPlayer.id, gf: [0,0], ga: [0,0]},
							secondPlayer: {id: sPlayer.id, gf: [0,0], ga: [0,0]},
							winner: '',
							gameId: 'g' + idGenerator(),
							gamePlayed: false
						});
						saveGroupMatches(groupMatchesData);
					}
				} else {
					if (fPlayer.id === filtered.winner) {
						result = fPlayer.player;
					} else if (sPlayer.id === filtered.winner) {
						result = sPlayer.player;
					}
				}
			} else if(players.includes(fPlayer.id) && players.includes(sPlayer.id) && filtered.gamePlayed === false) {
				if (fPlayer.gd > sPlayer.gd) {
					result = fPlayer.player;
				} else if (fPlayer.gd < sPlayer.gd) {
					result = sPlayer.player;
				}
			}
		});
	});

	if (result) return result;
};

compareMultiEqualScores = (groupPlayersCopy, index, groupMatchesData) => {
	groupPlayersCopy[index].players.sort((a, b) => {
		let fPlayer = new Player(a);
		let sPlayer = new Player(b);

		if(fPlayer.score === sPlayer.score) {
			if (fPlayer.gd < sPlayer.gd) {
				return 1;
			} else if (fPlayer.gd === sPlayer.gd) {
				if (fPlayer.gf < sPlayer.gf) {
					return 1;
				} else if (fPlayer.gf === sPlayer.gf && checkGroupMatchesFinished(groupMatchesData)) {
					groupMatchesData[index].games.push({
						firstPlayer: {
							id: fPlayer.id,
							gf: [0,0],
							ga: [0,0]
						},
						secondPlayer: {
							id: sPlayer.id,
							gf: [0,0],
							ga: [0,0]
						},
						winner: '',
						gameId: 'g' + idGenerator(),
						gamePlayed: false
					});

					saveGroupMatches(groupMatchesData);
				} else {
					return -1;
				}
			} else {
				return -1;
			}
		}
	});
};

sortTopPlayers = (groupPlayers, groupMatchesData) => {
	/*let obj = {
		groupId: null,
		player1Id: null,
		player2ID: null
	};

	function bubble(arr) {
		for (let k = 0; k < arr.length; k++) {
			let len = arr[k].players.length;

			for (let i = 0; i < len; i++) {
				for (let j = 0; j < len - 1; j++) {
					let fPlayer = new Player(arr[k].players[j]);
					let sPlayer = new Player(arr[k].players[j + 1]);

					if (fPlayer.score < sPlayer.score) {
						let temp = arr[k].players[j];
						arr[k].players[j] = arr[k].players[j + 1];
						arr[k].players[j + 1] = temp;
					} else if (fPlayer.score === sPlayer.score) {
						obj = {
							groupId: arr[k].groupId,
							player1Id: arr[k].players[j].id,
							player2Id: arr[k].players[j + 1].id
						}
					}else {

					}
				}
			}
		}

		return arr;
	}

	bubble(groupPlayersCopy);

	if (obj.groupId) {
		console.log('currentGroupId', obj);
		console.log('games array', groupMatchesData[obj.groupId].games);

		let foundedGame = groupMatchesData[obj.groupId].games.find(game => {
			return game.firstPlayer.id == obj.player1Id && game.secondPlayer.id == obj.player2Id
			|| game.firstPlayer.id == obj.player2Id && game.secondPlayer.id == obj.player1Id
		});

		console.log('foundedGame', foundedGame);

	}*/
	let sortCount = groupPlayers[0].players.length > 2 ? [0,1] : [0];

	let groupPlayersCopy = groupPlayers;

	let sortedPlayers = [
		{groupId: 0, players: []},
		{groupId: 1, players: []}
	];

	groupPlayersCopy.forEach((playersArray, index) => {
		let fPlayer, sPlayer, tPlayer;

		let newPlayersArray = {...playersArray};

		groupPlayers[0].players.forEach(() => {
			fPlayer = new Player(newPlayersArray.players[0]);
			sPlayer = new Player(newPlayersArray.players[1]);

			let tPlayerExist = newPlayersArray.players[2];

			if(tPlayerExist) {
				tPlayer = new Player(newPlayersArray.players[2]);
			}

			if (fPlayer.score > sPlayer.score) {
				sortedPlayers[index].players.push(fPlayer.player);

				newPlayersArray.players = newPlayersArray.players.filter(filtered => filtered.id !== fPlayer.id);
			}

			if (fPlayer.score === sPlayer.score && tPlayerExist ? sPlayer.score > tPlayer.score : false) {
				let compareResult = compareEqualScores(groupMatchesData, fPlayer.player, sPlayer.player);

				if (compareResult) {
					sortedPlayers[index].players.push(compareResult);

					newPlayersArray.players = newPlayersArray.players.filter(filtered => filtered.id !== compareResult.id);
				}
			} else {
				compareMultiEqualScores(groupPlayersCopy, index, groupMatchesData);
			}
		});

		newPlayersArray.players.forEach(player => {
			sortedPlayers[index].players.push(player);
		})
	});

	return sortedPlayers;
};

checkGenerateButton = (dataLength) => {
	if (dataLength) {
		document.querySelector('.generate-teams-button').classList.remove('hide');
	} else {
		document.querySelector('.generate-teams-button').classList.add('hide');
	}
};

checkGroupMatchesFinished = (data) => {
	let generateSemifinalMatchesButton = document.querySelector('.generate-semifinal-matches');

	if(data.length !== 0) {
		let gamePlayedEnd = [];

		data.forEach(group => {
			gamePlayedEnd = [...group.games.filter(filtered => (filtered.gamePlayed === false)), ...gamePlayedEnd];
		});

		if (gamePlayedEnd.length === 0) {
			generateSemifinalMatchesButton.classList.remove('hidden');
			return true;
		} else {
			generateSemifinalMatchesButton.classList.add('hidden');
			return false;
		}
	}else {
		generateSemifinalMatchesButton.classList.add('hidden');
		return false;
	}
};

checkGroupMatchesPlayed = (groupMatches) => {
	let gamePlayed = [];

	groupMatches.length > 0 && groupMatches.forEach(group => {
		gamePlayed = [...group.games.filter(filtered => (filtered.gamePlayed === true)), ...gamePlayed];
	});

	return gamePlayed.length !== 0;
};

checkMatchesFinished = (data) => {
	let generateFinalMatchesButton = document.querySelector('.generate-final-matches');

	if(data.length !== 0) {
		let gamePlayedEnd = [];

		gamePlayedEnd = [...data.filter(filtered => (filtered.gamePlayed === false)), ...gamePlayedEnd];

		if (gamePlayedEnd.length === 0) {
			generateFinalMatchesButton.classList.remove('hidden');
			return true;
		} else {
			generateFinalMatchesButton.classList.add('hidden');
			return false;
		}
	} else {
		generateFinalMatchesButton.classList.add('hidden');
		return false;
	}
};

checkMatchesPlayed = (data) => {
	let gamePlayed = [];

	data.length > 0 ? gamePlayed = [...data.filter(filtered => (filtered.gamePlayed === true)), ...gamePlayed] : [];

	return gamePlayed.length !== 0;
};

sortFinalPlayers = (finalPlayers) => {
	let sortedFinalPlayers = [];

	let calculateWin = (player1, player2) => {
		if(player1.win > player2.win) {
			sortedFinalPlayers.push(player1, player2);
		} else {
			sortedFinalPlayers.push(player2, player1);
		}
	};
	calculateWin(finalPlayers[0], finalPlayers[1]);
	calculateWin(finalPlayers[2], finalPlayers[3]);

	return sortedFinalPlayers;
};

getData = (url, eventType) => {
	dataAjax = (url) => {
		return fetch(url)
			.then(response => response.json())
			.catch(error => {
				console.error(error);
			});
	};

	dataAjax(url, eventType).then(data => {
		let event = new CustomEvent(eventType, {
			detail: {
				data
			}
		});

		window.dispatchEvent(event);
	});
};

getData(playerUrl, "player");
