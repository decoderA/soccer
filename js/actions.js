resetGame = () => {
	let generateButton = document.querySelector('.generate-teams-button');
	generateButton.disabled = false;
	generateButton.classList.remove('inactive');
	document.querySelector('.players-group').classList.remove('disabled');

	let data = {
		data: [],
		file: 'data/players.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		saveGroupMatches([]);
		saveGroupPlayers([]);
		saveFinalMatches([]);
		saveFinalPlayers([]);
		saveSemifinalMatches([]);
		saveSemifinalPlayers([]);
		setSiteConfigToLocalStorage({});

		let url = './read.php?path=./data/players.json';

		dataAjax(url).then(data => {
			let event = new CustomEvent("player", {
				detail: {
					data,
					functionCalled: 'reset'
				}
			});

			window.dispatchEvent(event);
		});
	});
};

saveSoccerPlayers = () => {
	let firstPlayers = playersState[0].players;
	let secondPlayers = playersState[1].players;

	if (firstPlayers.length !== 0 && secondPlayers.length !== 0) {
		if (firstPlayers.length === secondPlayers.length) {
			let playersArray = [
				{
					players: firstPlayers
				},
				{
					players: secondPlayers
				}
			];

			let data = {
				data: playersArray,
				file: 'data/players.json'
			};

			fetch('write.php', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(data),
			}).then(response => {
				let url = './read.php?path=./data/players.json';

				dataAjax(url).then(data => {
					let event = new CustomEvent("player", {
						detail: {
							data,
							functionCalled: 'saveScorePlayers'
						}
					});

					window.dispatchEvent(event);
				});
			});
		} else {
			alert('teams count not equal');
		}
	} else {
		alert('Please add perssons')
	}
};

generateGroupMatches = () => {
	saveSoccerPlayers();

	let firstGroup = playersState[0].players;
	let secondGroup = playersState[1].players;

	if (firstGroup.length === secondGroup.length && firstGroup.length % 2 === 0) {
		let groupMatches = generateRandomGroups(playersState);

		saveGroupMatches(groupMatches);
		saveGroupPlayers(groupMatches);
	} else {
		alert('Each soccer players group should have minimum 2 soccer players and groups should be equal and players count should be even');
	}
};

saveGroupMatches = (groupMatches) => {
	let gamePlayed = checkGroupMatchesPlayed(groupMatches);

	let data = {
		data: groupMatches,
		file: 'data/group-matches.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		let url = './read.php?path=./data/group-matches.json';

		dataAjax(url).then(data => {
			gamePlayed && updateGroupScoreTable(groupMatches);
		});
	});
};

saveGroupPlayers = (groupMatches) => {
	let groupPLayersArray = [];

	groupMatches.length > 0 && groupMatches.forEach(item => {
		groupPLayersArray = [...groupPLayersArray, item.groupPlayers];
	});

	let groupPlayers = [
		{
			groupId: 0,
			players: []
		},
		{
			groupId: 1,
			players: []
		}
	];

	groupPLayersArray.forEach((playersArray, index) => {
		playersArray.forEach(id => {
			let newPlayer = new Player();
			newPlayer.id = id;
			newPlayer.name = getNameById(playersState, id);
			groupPlayers[index].players.push(newPlayer.player);
		});
	});

	let data = {
		data: groupMatches.length === 0 ? [] : groupPlayers,
		file: 'data/group-players.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		let url = './read.php?path=./data/group-matches.json';

		dataAjax(url).then(data => {
			let event = new CustomEvent("teams", {
				detail: {
					data,
					functionCalled: 'saveGroupPlayers'
				}
			});

			window.dispatchEvent(event);
		});
	});
};

updateGroupMatches = (event) => {
	let groupMatchesUrl = './read.php?path=./data/group-matches.json';
	let gameId = event.target.getAttribute('data-game-id');
	let fields = document.querySelectorAll('.group-blocks input[data-game-id="' + gameId + '"]');
	let groupId = parseInt(getStringNumber(event.target.closest('.group-wrap').getAttribute('data-team')));

	let fPlayerValue = parseInt(fields[0].value);
	let fPlayerValue2 = parseInt(fields[2].value);
	let fPlayerId = fields[0].getAttribute('data-player-id');

	let sPlayerValue = parseInt(fields[1].value);
	let sPlayerValue2 = parseInt(fields[3].value);
	let sPlayerId = fields[1].getAttribute('data-player-id');

	if (fPlayerValue > 0 || sPlayerValue > 0 && fPlayerValue2 > 0 || sPlayerValue2 > 0) {
		dataAjax(groupMatchesUrl).then(data => {
			let groupMatchesArray = JSON.parse(data);
			let gameIndex = null;

			let group = groupMatchesArray[groupId];

			group.games.filter(filtered => {
				if(filtered.gameId === gameId) {
					gameIndex = group.games.indexOf(filtered)
				}
			});

			if(group.gamesCount !== group.games.length && gameIndex < group.gamesCount) {
				let removedGames = group.games.length - group.gamesCount;

				for(let i = group.gamesCount - 1; i < removedGames; i++) {
					group.games.pop();
				}

				saveSemifinalMatches([]);
				saveSemifinalPlayers([]);
				saveFinalMatches([]);
				saveFinalPlayers([]);
			}

			if(checkGroupMatchesFinished(groupMatchesArray)) {
				saveSemifinalMatches([]);
				saveSemifinalPlayers([]);
				saveFinalMatches([]);
				saveFinalPlayers([]);
			}

			groupMatchesArray.forEach((gamesArray, index) => {
				gamesArray.games.forEach(game => {
					if (game.gameId === gameId) {
						if (fPlayerValue > sPlayerValue && fPlayerValue2 > sPlayerValue2) {
							game.winner = fPlayerId;
						} else if (fPlayerValue > sPlayerValue && fPlayerValue2 < sPlayerValue2 || fPlayerValue < sPlayerValue && fPlayerValue2 > sPlayerValue2) {
							game.winner = '0';
						} else if(fPlayerValue < sPlayerValue && fPlayerValue2 < sPlayerValue2) {
							game.winner = sPlayerId;
						} else if(fPlayerValue === sPlayerValue || fPlayerValue2 === sPlayerValue2) {
							alert(`the goals can't be equal`);

							return false;
						}

						game.gamePlayed = true;

						game.firstPlayer.gf = [fPlayerValue, fPlayerValue2];
						game.firstPlayer.ga = [sPlayerValue, sPlayerValue2];

						game.secondPlayer.gf = [sPlayerValue, sPlayerValue2];
						game.secondPlayer.ga = [fPlayerValue, fPlayerValue2];
					}
				});
			});

			saveGroupMatches(groupMatchesArray);
		});
	}
};

editGroupMatches = (event) => {
	let gameId = event.target.getAttribute('data-game-id');
	let elements = document.querySelectorAll('[data-game-id="' + gameId + '"]');
	let url = './read.php?path=./data/group-matches.json';
	let groupId = parseInt(getStringNumber(event.target.closest('.group-wrap').getAttribute('data-team')));

	dataAjax(url).then(data => {
		let groupMatches = JSON.parse(data);
		let group = groupMatches[groupId];

		if(group.gamesCount !== group.games.length || checkGroupMatchesFinished(groupMatches)) {
			let confirmEdit = confirm("If you will press ok button semifinal and final games will be removed and added games also.");

			if(!confirmEdit) {
				return false;
			}
		}

		elements.forEach(item => {
			if (item.hasAttribute('data-button')) {
				item.disabled = true;
				item.classList.add('disabled');
			} else {
				item.disabled = false;
				item.classList.remove('disabled');
			}
		});
	});
};

createGroupScoreTable = () => {
	let getGroupPlayersUrl = './read.php?path=./data/group-players.json';

	dataAjax(getGroupPlayersUrl).then(data => {
		let groupPlayers = JSON.parse(data);
		let wrap;
		let playerInfo = '';
		let tableHead = '<table>' + tHead + '<tbody></tbody></table>';

		groupPlayers.forEach((playersArray, index) => {
			wrap = document.querySelector('[data-score="group-' + index + '"]');

			wrap.innerHTML = '';

			wrap.insertAdjacentHTML('beforeend', tableHead);

			playerInfo = '';

			playersArray.players.forEach((player, index) => {
				let top = '';
				let newPlayer = new Player(player);

				if (index < 2 && newPlayer.score > 0) top = 'top';

				playerInfo += '<tr data-player-id="' + player.id + '" class="' + top + '-' + index + '">' + newPlayer.tBody + '</tr>';
			});

			wrap.children[0].children[1].insertAdjacentHTML('beforeend', playerInfo);
		});
	});
};

updateGroupScoreTable = (groupMatchesData) => {
	let getGroupPlayersUrl = './read.php?path=./data/group-players.json';

	dataAjax(getGroupPlayersUrl).then(data => {
		let groupPlayers = JSON.parse(data);

		groupPlayers.forEach(playersArray => {
			playersArray.players.forEach(player => {
				let finalScores = getGroupPlayerMatches(groupMatchesData, player.id);

				if (finalScores) {
					Object.assign(player, finalScores);
				}
			});

			playersArray.players.sort((a, b) => {
				let fPlayer = new Player(a);
				let sPlayer = new Player(b);

				if (fPlayer.score < sPlayer.score) {
					return 1;
				} else {
					return -1;
				}
			});
		});

		let sortResult = sortTopPlayers(groupPlayers, groupMatchesData);

		let playersData = {
			data: sortResult,
			file: 'data/group-players.json'
		};

		fetch('write.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(playersData),
		}).then(response => {
			let url = './read.php?path=./data/group-matches.json';


			dataAjax(url).then(data => {
				let event = new CustomEvent("teams", {
					detail: {
						data,
						functionCalled: 'updateGroupScoreTable'
					}
				});

				window.dispatchEvent(event);
			});
		});
	});
};

/************************************Semifinal*************************************/

generateSemifinal = () => {
	let groupPlayersUrl = './read.php?path=./data/group-players.json';
	let generateSemifinalButton = document.querySelector('.generate-semifinal-matches');

	dataAjax(groupPlayersUrl).then(data => {
		let groupPlayers = JSON.parse(data);

		let semiFinalGames = [];

		semiFinalGames.push({
			firstPlayer: {id: groupPlayers[0].players[0].id, gf: [0], ga: [0]},
			secondPlayer: {id: groupPlayers[1].players[1].id, gf: [0], ga: [0]},
			winner: '',
			gameId: 'g' + idGenerator(),
			gamePlayed: false
		});

		semiFinalGames.push({
			firstPlayer: {id: groupPlayers[1].players[0].id, gf: [0], ga: [0]},
			secondPlayer: {id: groupPlayers[0].players[1].id, gf: [0], ga: [0]},
			winner: '',
			gameId: 'g' + idGenerator(),
			gamePlayed: false
		});

		generateSemifinalButton.disabled = true;
		generateSemifinalButton.classList.add('inactive');

		saveSemifinalMatches(semiFinalGames);
		saveSemifinalPlayers(semiFinalGames);
	});
};

saveSemifinalMatches = (semiFinalGames) => {
	let gamePlayed = checkMatchesPlayed(semiFinalGames);

	let data = {
		data: semiFinalGames,
		file: 'data/semifinal-matches.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		let url = './read.php?path=./data/semifinal-matches.json';

		dataAjax(url).then(data => {
			gamePlayed && updateSemifinalScoreTable(semiFinalGames);
		});
	});
};

saveSemifinalPlayers = (semiFinalGames) => {
	let semifinalPLayersArray = [];

	semiFinalGames.length > 0 && semiFinalGames.forEach(player => {
		let newFirstPlayer = new Player();
		newFirstPlayer.id = player.firstPlayer.id;
		newFirstPlayer.name = getNameById(playersState, player.firstPlayer.id);
		newFirstPlayer.player.gameId = player.gameId;

		let newSecondPlayer = new Player();
		newSecondPlayer.id = player.secondPlayer.id;
		newSecondPlayer.name = getNameById(playersState, player.secondPlayer.id);
		newSecondPlayer.player.gameId = player.gameId;

		semifinalPLayersArray.push(newFirstPlayer.player, newSecondPlayer.player);
	});


	let data = {
		data: semiFinalGames.length > 0 ? semifinalPLayersArray : [],
		file: 'data/semifinal-players.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		let url = './read.php?path=./data/semifinal-matches.json';

		dataAjax(url).then(data => {
			let event = new CustomEvent("semifinal", {
				detail: {
					data,
					functionCalled: 'saveSemifinalPlayers'
				}
			});

			window.dispatchEvent(event);
		});
	});
};

createSemifinalScoreTable = () => {
	let getSemifinalPlayersUrl = './read.php?path=./data/semifinal-players.json';

	dataAjax(getSemifinalPlayersUrl).then(data => {
		let semifinalPlayers = JSON.parse(data);
		let index = 0;
		let playerInfo = '';

		for(let i=0; i < semifinalPlayers.length; i += 2) {
			let wrap = document.querySelector('[data-score="semifinal-group-' + index + '"]');

			getNewPlayer = (index) => {
				return new Player(semifinalPlayers[index]).list;
			};

			playerInfo = `<ul>
							${getNewPlayer(i)} ${getNewPlayer(i+1)}
						</ul>`;

			wrap.insertAdjacentHTML('afterbegin', playerInfo);
			index += 1;
		}
	});
};

updateSemifinalMatches = (event) => {
	let semifinalMatchesUrl = './read.php?path=./data/semifinal-matches.json';
	let gameId = event.target.getAttribute('data-game-id');
	let fields = document.querySelectorAll('.semifinal-wrap input[data-game-id="' + gameId + '"]');
	let firstPlayerValue = parseInt(fields[0].value);
	let secondPlayerValue = parseInt(fields[1].value);
	let Player1Id = fields[0].getAttribute('data-player-id');
	let Player2Id = fields[1].getAttribute('data-player-id');

	if (firstPlayerValue > 0 || secondPlayerValue > 0) {
		if(firstPlayerValue !== secondPlayerValue) {
			dataAjax(semifinalMatchesUrl).then(data => {
				let semifinalMatchesArray = JSON.parse(data);

				semifinalMatchesArray.forEach(game => {
					if (game.gameId === gameId) {
						if (firstPlayerValue > secondPlayerValue) {
							game.winner = Player1Id;
						} else if (firstPlayerValue === secondPlayerValue) {
							game.winner = '0';
						} else {
							game.winner = Player2Id;
						}
						game.gamePlayed = true;
						game.firstPlayer.gf = [firstPlayerValue];
						game.firstPlayer.ga = [secondPlayerValue];
						game.secondPlayer.gf = [secondPlayerValue];
						game.secondPlayer.ga = [firstPlayerValue];
					}
				});

				saveSemifinalMatches(semifinalMatchesArray);
			});
		}else {
			alert(`Semifinal Match can't be draw play again.`);
		}
	}

};

editSemifinalMatches = (event) => {
	let gameId = event.target.getAttribute('data-game-id');
	let elements = document.querySelectorAll('.semifinal-wrap [data-game-id="' + gameId + '"]');
	let url = './read.php?path=./data/semifinal-matches.json';

	dataAjax(url).then(data => {
		let groupMatches = JSON.parse(data);

		if(checkMatchesFinished(groupMatches)) {
			let confirmEdit = confirm("If you will press ok button final games will be removed and added games also.");

			if(!confirmEdit) {
				return false;
			}
		}

		elements.forEach(item => {
			if (item.hasAttribute('data-button')) {
				item.disabled = true;
				item.classList.add('disabled');
			} else {
				item.disabled = false;
				item.classList.remove('disabled');
			}
		});
	});
};

updateSemifinalScoreTable = (semiFinalGames) => {
	let getSemifinalPlayersUrl = './read.php?path=./data/semifinal-players.json';

	dataAjax(getSemifinalPlayersUrl).then(data => {
		let semifinalPlayers = JSON.parse(data);

		semifinalPlayers.forEach(player => {
			let finalScores = getPlayerMatches(semiFinalGames, player.id);

			if (finalScores) {
				Object.assign(player, finalScores);
			}
		});

		semifinalPlayers.sort((a, b) => {
			let fPlayer = new Player(a);
			let sPlayer = new Player(b);

			if(fPlayer.gameId === sPlayer.gameId) {
				if (fPlayer.score < sPlayer.score) {
					return 1;
				} else {
					return -1;
				}
			}
		});

		let playersData = {
			data: semifinalPlayers,
			file: 'data/semifinal-players.json'
		};

		fetch('write.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(playersData),
		}).then(response => {
			let url = './read.php?path=./data/semifinal-matches.json';

			dataAjax(url).then(data => {
				let event = new CustomEvent("semifinal", {
					detail: {
						data,
						functionCalled: 'updateSemifinalScoreTable'
					}
				});

				saveFinalMatches([]);
				saveFinalPlayers([]);

				window.dispatchEvent(event);
			});
		});
	});
};

/************************************Final*************************************/

generateFinal = () => {
	let semiFinalPlayersUrl = './read.php?path=./data/semifinal-players.json';
	let generateFinalButton = document.querySelector('.generate-final-matches');

	dataAjax(semiFinalPlayersUrl).then(data => {
		let semiFinalPlayers = JSON.parse(data);

		let finalGames = [];

		finalGames.push({
			firstPlayer: {id: semiFinalPlayers[0].id, gf: [0], ga: [0]},
			secondPlayer: {id: semiFinalPlayers[2].id, gf: [0], ga: [0]},
			winner: '',
			gameId: 'g' + idGenerator(),
			gamePlayed: false
		});

		finalGames.push({
			firstPlayer: {id: semiFinalPlayers[1].id, gf: [0], ga: [0]},
			secondPlayer: {id: semiFinalPlayers[3].id, gf: [0], ga: [0]},
			winner: '',
			gameId: 'g' + idGenerator(),
			gamePlayed: false
		});

		generateFinalButton.disabled = true;
		generateFinalButton.classList.add('inactive');

		saveFinalMatches(finalGames);
		saveFinalPlayers(finalGames);
	});
};

saveFinalMatches = (finalGames) => {
	let gamePlayed = checkMatchesPlayed(finalGames);

	let data = {
		data: finalGames,
		file: 'data/final-matches.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		let url = './read.php?path=./data/final-matches.json';

		dataAjax(url).then(data => {
			gamePlayed && updateFinalScoreTable(finalGames);
		});
	});
};

saveFinalPlayers = (finalGames) => {
	let finalPLayersArray = [];

	finalGames.length > 0 && finalGames.forEach(player => {
		let newFirstPlayer = new Player();
		newFirstPlayer.id = player.firstPlayer.id;
		newFirstPlayer.name = getNameById(playersState, player.firstPlayer.id);

		let newSecondPlayer = new Player();
		newSecondPlayer.id = player.secondPlayer.id;
		newSecondPlayer.name = getNameById(playersState, player.secondPlayer.id);

		finalPLayersArray.push(newFirstPlayer.player, newSecondPlayer.player);
	});

	let data = {
		data: finalGames.length > 0 ? finalPLayersArray: [],
		file: 'data/final-players.json'
	};

	fetch('write.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	}).then(response => {
		let url = './read.php?path=./data/final-matches.json';

		dataAjax(url).then(data => {
			let event = new CustomEvent("final", {
				detail: {
					data,
					functionCalled: 'saveFinalPlayers'
				}
			});

			window.dispatchEvent(event);
		});
	});
};

createFinalScoreTable = () => {
	let getFinalPlayersUrl = './read.php?path=./data/final-players.json';

	dataAjax(getFinalPlayersUrl).then(data => {
		let finalPlayers = JSON.parse(data);

		let index = 0;
		let playerInfo = '';

		for(let i=0; i < finalPlayers.length; i += 2) {
			let wrap = document.querySelector('[data-score="final-group-' + index + '"]');

			getNewPlayer = (index) => {
				return new Player(finalPlayers[index]).list;
			};

			playerInfo = `<ul>
							${getNewPlayer(i)} ${getNewPlayer(i+1)}
						</ul>`;

			wrap.insertAdjacentHTML('beforeend', playerInfo);
			index += 1;
		}
	});
};

updateFinalMatches = (event) => {
	let finalMatchesUrl = './read.php?path=./data/final-matches.json';
	let gameId = event.target.getAttribute('data-game-id');
	let fields = document.querySelectorAll('.final-wrap input[data-game-id="' + gameId + '"]');
	let firstPlayerValue = parseInt(fields[0].value);
	let secondPlayerValue = parseInt(fields[1].value);
	let Player1Id = fields[0].getAttribute('data-player-id');
	let Player2Id = fields[1].getAttribute('data-player-id');

	if (firstPlayerValue > 0 || secondPlayerValue > 0) {
		if(firstPlayerValue !== secondPlayerValue) {
			dataAjax(finalMatchesUrl).then(data => {
				let finalMatchesArray = JSON.parse(data);

				finalMatchesArray.forEach(game => {
					if (game.gameId === gameId) {
						if (firstPlayerValue > secondPlayerValue) {
							game.winner = Player1Id;
						} else if (firstPlayerValue === secondPlayerValue) {
							game.winner = '0';
						} else {
							game.winner = Player2Id;
						}
						game.gamePlayed = true;
						game.firstPlayer.gf = [firstPlayerValue];
						game.firstPlayer.ga = [secondPlayerValue];
						game.secondPlayer.gf = [secondPlayerValue];
						game.secondPlayer.ga = [firstPlayerValue];
					}
				});

				saveFinalMatches(finalMatchesArray);
			});
		}else {
			alert(`Final Match can't be draw play again.`);
		}
	}
};

editFinalMatches = (event) => {
	let gameId = event.target.getAttribute('data-game-id');
	let elements = document.querySelectorAll('.final-wrap [data-game-id="' + gameId + '"]');

	elements.forEach(item => {
		if (item.hasAttribute('data-button')) {
			item.disabled = true;
			item.classList.add('disabled');
		} else {
			item.disabled = false;
			item.classList.remove('disabled');
		}
	});
};

updateFinalScoreTable = (finalGames) => {
	let getFinalPlayersUrl = './read.php?path=./data/final-players.json';

	dataAjax(getFinalPlayersUrl).then(data => {
		let finalPlayers = JSON.parse(data);

		finalPlayers.forEach(player => {
			let finalScores = getPlayerMatches(finalGames, player.id);

			if (finalScores) {
				Object.assign(player, finalScores);
			}
		});

		finalPlayers = sortFinalPlayers(finalPlayers);

		let playersData = {
			data: finalPlayers,
			file: 'data/final-players.json'
		};

		fetch('write.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(playersData),
		}).then(response => {
			let url = './read.php?path=./data/final-matches.json';

			dataAjax(url).then(data => {
				let event = new CustomEvent("final", {
					detail: {
						data,
						functionCalled: 'updateFinalScoreTable'
					}
				});

				window.dispatchEvent(event);
			});
		});
	});
};

showWinners = () => {
	let getFinalPlayersUrl = './read.php?path=./data/final-players.json';
	let pedestal = document.querySelector('.pedestal');
	let pedestalWrap = document.querySelector('.pedestal-wrap');

	dataAjax(getFinalPlayersUrl).then(data => {
		let finalPlayers = JSON.parse(data);

		pedestal.classList.remove('hide');

		pedestalWrap.innerHTML = '';

		pedestalWrap.insertAdjacentHTML('beforeend', '<div class="first-place place '+ finalPlayers[0].name +' ">' + finalPlayers[0].name + '</div>');
		pedestalWrap.insertAdjacentHTML('beforeend', '<div class="second-place place '+ finalPlayers[1].name +' ">' + finalPlayers[1].name + '</div>');
		pedestalWrap.insertAdjacentHTML('beforeend', '<div class="third-place place '+ finalPlayers[2].name +' ">' + finalPlayers[2].name + '</div>');
	});

	setTimeout(() => {
		window.scrollTo(0,document.body.scrollHeight);
	}, 200);
};

collapseGroupMatches = (event) => {
	let group = event.target.closest('.collapse-wrap');
	let groupName = group.getAttribute('data-wrap');
	let soccerConfig = getSiteConfigFromLocalStorage() ? getSiteConfigFromLocalStorage() : null;


	if(!soccerConfig || !soccerConfig[groupName + '-collapsed']) {
		setSiteConfigToLocalStorage({[groupName + '-collapsed']: true});
		group.classList.add('collapsed');
	} else if(soccerConfig[groupName + '-collapsed'] === false) {
		setSiteConfigToLocalStorage({[groupName + '-collapsed']: true});
		group.classList.add('collapsed');
	}else {
		setSiteConfigToLocalStorage({[groupName + '-collapsed']: false});
		group.classList.remove('collapsed');
	}
};