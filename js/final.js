window.addEventListener("final", function (e) {
	console.log('final event listened---', e.detail.functionCalled);

	let data = JSON.parse(e.detail.data);
	let dataLength = data.length;
	let getSiteConfig = getSiteConfigFromLocalStorage();
	let finalMatchesWrap = document.querySelector('.final-matches-wrap');
	let generateFinalButton = document.querySelector('.generate-final-matches');
	let pedestal = document.querySelector('.pedestal');
	let pedestalWrap = document.querySelector('.pedestal-wrap');
	let curWrapHtml, groupListsWrapHtml;

	pedestal.classList.add('hide');
	pedestalWrap.innerHTML = '';
	finalMatchesWrap.innerHTML = '';
	groupListsWrapHtml = '';
	curWrapHtml = '';

	if(dataLength) {
		generateFinalButton.disabled = true;
		generateFinalButton.classList.add('inactive');

		data.forEach((match, index) => {
			let finished = '';
			let message = '';
			let edit = 'disabled';

			groupListsWrapHtml = `
				<div class="final-group-wrap" data-final-team="group-${index}">
					<div class="match-wrap" id="final-group-${index}"></div>
				</div>`;

			finalMatchesWrap.insertAdjacentHTML('beforeend', groupListsWrapHtml);

			let finalMatchWrap = document.querySelector('#final-group-' + index);

			finalMatchWrap.innerHTML = '';
			curWrapHtml = '';

			if (match.gamePlayed) {
				finished = 'disabled';
				edit = '';

				if (match.winner === '0') {
					message = `<span>The Match Draw</span>`;
				} else {
					message = `<span>The Winner is  ${getNameById(playersState, match.winner)}</span>`;
				}
			}

			curWrapHtml = getGameTemplate(match, finished, edit, message, 'single');

			finalMatchWrap.insertAdjacentHTML('afterbegin', curWrapHtml);
			finalMatchWrap.insertAdjacentHTML('beforebegin', `<div class="score-list" data-score="final-group-${index}"><h1>${index === 0 ? '1st, 2nd place match' : '3rd place match'}</h1></div>`);
		});

		finalMatchesWrap.insertAdjacentHTML('afterbegin', '<div class="title-wrap"><h1>Final Matches</h1><button class="button blue clear-radius thin collapse-button final-collapse-button">Collapse</button></div>');

		if(getSiteConfig && getSiteConfig['final-wrap-collapsed']) {
			finalMatchesWrap.classList.add('collapsed');
		}

		let submitResultButtonArray = document.querySelectorAll('.final-wrap .submit-result-button');

		submitResultButtonArray.forEach(submitResultButton => {
			submitResultButton.addEventListener('click', updateFinalMatches);
		});

		let editButtonArray = document.querySelectorAll('.final-wrap .edit-button');

		editButtonArray.forEach(submitResultButton => {
			submitResultButton.addEventListener('click', editFinalMatches);
		});

		let collapseButton = document.querySelector('.final-collapse-button');

		collapseButton.addEventListener('click', collapseGroupMatches);

		createFinalScoreTable();

		if(checkMatchesFinished(data)) {
			/*editButtonArray.forEach(submitResultButton => {
				submitResultButton.disabled = true;
				submitResultButton.classList.add('inactive');
			});*/

			showWinners();
		}
	}else {
		generateFinalButton.disabled = false;
		generateFinalButton.classList.remove('inactive');
	}

    let checkEvent = new CustomEvent("check", {
        detail: {
            loaded: true
        }
    });

    window.dispatchEvent(checkEvent);
});