window.addEventListener("teams", function (e) {
    console.log('teams event listened---', e.detail.functionCalled);

    let generateButton = document.querySelector('.generate-teams-button');
    let data = JSON.parse(e.detail.data);

    let dataLength = data.length;

    let getSiteConfig = getSiteConfigFromLocalStorage();
    let groupListsWrap = document.querySelector('.group-blocks');
    let groupListsWrapHtml = '';

    let curWrapHtml;

    groupListsWrap.innerHTML = '';
    if (dataLength) {
        document.querySelector('.players-group').classList.add('disabled');

        data.forEach((item, index) => {

            let games = item.games;

            groupListsWrapHtml = `
					<div class="group-wrap" data-team="group-${data[index].groupId}">
						<h1>${data[index].groupName}</h1>
						<div class="group-list-wrap" id="group-${data[index].groupId}"></div>
					</div>`;

            groupListsWrap.insertAdjacentHTML('beforeend', groupListsWrapHtml);


            let wrap = document.querySelector('#group-' + data[index].groupId);

            wrap.innerHTML = '';

            curWrapHtml = '';

            games.forEach(item => {
                let finished = '';
                let message = '';
                let edit = 'disabled';

                if (item.gamePlayed) {
                    generateButton.disabled = true;
                    generateButton.classList.add('inactive');
                    finished = 'disabled';
                    edit = '';

                    if (item.winner === '0') {
                        message = `<span>The Match Draw</span>`;
                    } else {
                        message = `<span>The Winner is  ${getNameById(playersState, item.winner)}</span>`;
                    }
                }

                curWrapHtml += getGameTemplate(item, finished, edit, message);
            });

            wrap.insertAdjacentHTML('afterbegin', curWrapHtml);
            wrap.insertAdjacentHTML('beforebegin', '<div class="score-list" data-score="group-' + data[index].groupId + '"></div>');
        });

        groupListsWrap.insertAdjacentHTML('afterbegin', '<div class="title-wrap"><h1>Group Matches</h1><button class="button blue clear-radius thin collapse-button group-collapse-button">Collapse</button></div>');

        if(getSiteConfig && getSiteConfig['group-wrap-collapsed']) {
            groupListsWrap.classList.add('collapsed');
        }

        let submitResultButtonArray = document.querySelectorAll('.group-blocks .submit-result-button');

        submitResultButtonArray.forEach(submitResultButton => {
            submitResultButton.addEventListener('click', updateGroupMatches);
        });

        let editButtonArray = document.querySelectorAll('.group-blocks .edit-button');

        editButtonArray.forEach(submitResultButton => {
            submitResultButton.addEventListener('click', editGroupMatches);
        });

        let collapseButton = document.querySelector('.group-collapse-button');

        collapseButton.addEventListener('click', collapseGroupMatches);

        createGroupScoreTable();

        checkGroupMatchesFinished(data);

        if (checkGroupMatchesFinished(data)) {
            /*editButtonArray.forEach(submitResultButton => {
                submitResultButton.disabled = true;
                submitResultButton.classList.add('inactive');
            });*/

            getData(semiFinalUrl, "semifinal");
        }
    } else {
        checkGroupMatchesFinished([]);
    }

    let checkEvent = new CustomEvent("check", {
        detail: {
            loaded: true
        }
    });

    window.dispatchEvent(checkEvent);
});