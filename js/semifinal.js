window.addEventListener("semifinal", function (e) {
	console.log('semifinal event listened---', e.detail.functionCalled);

	let data = JSON.parse(e.detail.data);
	let dataLength = data.length;
	let getSiteConfig = getSiteConfigFromLocalStorage();
	let semifinalMatchesWrap = document.querySelector('.semifinal-matches-wrap');
	let generateSemifinalButton = document.querySelector('.generate-semifinal-matches');
	let groupListsWrapHtml,curWrapHtml;

	groupListsWrapHtml = '';
	semifinalMatchesWrap.innerHTML = '';

	if (dataLength) {
		generateSemifinalButton.disabled = true;
		generateSemifinalButton.classList.add('inactive');

		data.forEach((match, index) => {
			let finished = '';
			let message = '';
			let edit = 'disabled';

			groupListsWrapHtml = `
				<div class="semifinal-group-wrap" data-semifinal-team="group-${index}">
					<div class="match-wrap" id="semifinal-group-${index}"></div>
				</div>`;

			semifinalMatchesWrap.insertAdjacentHTML('beforeend', groupListsWrapHtml);

			let semifinalMatchWrap = document.querySelector('#semifinal-group-' + index);

			semifinalMatchWrap.innerHTML = '';
			curWrapHtml = '';

			if (match.gamePlayed) {
				finished = 'disabled';
				edit = '';

				if (match.winner === '0') {
					message = `<span>The Match Draw</span>`;
				} else {
					message = `<span>The Winner is  ${getNameById(playersState, match.winner)}</span>`;
				}
			}

			curWrapHtml = getGameTemplate(match, finished, edit, message, 'single');

			semifinalMatchWrap.insertAdjacentHTML('afterbegin', curWrapHtml);
			semifinalMatchWrap.insertAdjacentHTML('beforebegin', '<div class="score-list" data-score="semifinal-group-' + index + '"></div>');
		});

		semifinalMatchesWrap.insertAdjacentHTML('afterbegin', '<div class="title-wrap"><h1>Semifinal Matches</h1><button class="button blue clear-radius thin collapse-button semifinal-collapse-button">Collapse</button></div>');

		if(getSiteConfig && getSiteConfig['semifinal-wrap-collapsed']) {
			semifinalMatchesWrap.classList.add('collapsed');
		}

		let submitResultButtonArray = document.querySelectorAll('.semifinal-wrap .submit-result-button');

		submitResultButtonArray.forEach(submitResultButton => {
			submitResultButton.addEventListener('click', updateSemifinalMatches);
		});

		let editButtonArray = document.querySelectorAll('.semifinal-wrap .edit-button');

		editButtonArray.forEach(submitResultButton => {
			submitResultButton.addEventListener('click', editSemifinalMatches);
		});

		let collapseButton = document.querySelector('.semifinal-collapse-button');

		collapseButton.addEventListener('click', collapseGroupMatches);


		checkMatchesFinished(data);

		createSemifinalScoreTable();

		if (checkMatchesFinished(data)) {
			/*editButtonArray.forEach(submitResultButton => {
				submitResultButton.disabled = true;
				submitResultButton.classList.add('inactive');
			});*/

			getData(finalUrl, "final");
		}
	} else {
		checkMatchesFinished([]);
		generateSemifinalButton.disabled = false;
		generateSemifinalButton.classList.remove('inactive');
	}

	let checkEvent = new CustomEvent("check", {
		detail: {
			loaded: true
		}
	});

	window.dispatchEvent(checkEvent);
});