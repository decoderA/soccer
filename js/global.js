const winScore = 3;
const tieScore = 1;

const tHead = `<thead>
		<tr>
			<th>Name</th>
			<th>GP</th>
			<th>W</th>
			<th>T</th>
			<th>L</th>
			<th>GF</th>
			<th>GA</th>
			<th>GD</th>
			<th>Score</th>
		</tr>
	</thead>`;

getGameTemplate = (item, finished, edit, message, single) => {
	let multiple;

	if(!single) {
		multiple = `<ul>
							<li>
								<img src="./images/tshirt2.svg" alt="">
								<span>${getNameById(playersState, item.firstPlayer.id)}
									<input ${finished} type="number" class="clear-radius bordered ${finished}" 
											value="${item.firstPlayer.gf[1]}" placeholder="0" 
											data-game-id="${item.gameId}" data-player-id="${item.firstPlayer.id}" 
									/>
								</span> 
							</li>
							<li>vs</li>
							<li>
								<span>${getNameById(playersState, item.secondPlayer.id)}
									<input ${finished} type="number" class="clear-radius bordered ${finished}" 
											value="${item.secondPlayer.gf[1]}" placeholder="0" 
											data-game-id="${item.gameId}" data-player-id="${item.secondPlayer.id}" 
										/>
								</span>
								<img src="./images/tshirt1.svg" alt="">
							</li>
						</ul>`;
	}

	return  ` <div class="opponents-wrap ${finished}">
					<img src="./images/soccer-field.jpg" alt="" />
					<div class="overlay"><h4>opponents</h4>
						<div class="wrap">
							<ul>
								<li>
									<img src="./images/tshirt1.svg" alt="">
									<span>${getNameById(playersState, item.firstPlayer.id)}
										<input ${finished} type="number" class="clear-radius bordered ${finished}" 
												value="${item.firstPlayer.gf[0]}" placeholder="0" 
												data-game-id="${item.gameId}" data-player-id="${item.firstPlayer.id}" 
										/>
									</span> 
								</li>
								<li>vs</li>
								<li>
									<span>${getNameById(playersState, item.secondPlayer.id)}
										<input ${finished} type="number" class="clear-radius bordered ${finished}" 
												value="${item.secondPlayer.gf[0]}" placeholder="0" 
												data-game-id="${item.gameId}" data-player-id="${item.secondPlayer.id}" 
											/>
									</span>
									<img src="./images/tshirt2.svg" alt="">
								</li>
							</ul>
							${multiple ? multiple : ''}
						</div>
						<button class="blue clear-radius submit-result-button thin ${finished}" ${finished} data-game-id="${item.gameId}">submit results</button>
						<button class="blue clear-radius edit-button thin ${edit}" ${edit} data-game-id="${item.gameId}" data-button="edit">Edit</button>
					</div>
					<div class="message">${message}</div> 
				</div>`;
};

class Player {
	constructor(player = Player.getDefaultPlayer()) {
		this._player = player;
	}

	get player() {
		return this._player;
	}

	get id() {
		return this._player.id;
	}

	set id(value) {
		this._player.id = value;
	}

	get name() {
		return this._player.name;
	}

	set name(value) {
		this._player.name = value;
	}

	get win() {
		return this._player.win;
	}

	set win(value) {
		this._player.win = value;
	}

	get loose() {
		return this._player.loose;
	}

	set loose(value) {
		this._player.loose = value;
	}

	get tie() {
		return this._player.tie;
	}

	set tie(value) {
		this._player.tie = value;
	}

	get gf() {
		return this._player.gf[1] ? this._player.gf[0] + this._player.gf[1] : this._player.gf[0];
	}

	set gf(value) {
		this._player.gf = value[1] ? [value[0] + this._player.gf[0], value[1] + this._player.gf[1]] : [value[0] + this._player.gf[0]];
	}

	get ga() {
		return this._player.ga[1] ? this._player.ga[0] + this._player.ga[1] : this._player.ga[0];
	}

	set ga(value) {
		this._player.ga = value[1] ? [value[0] + this._player.ga[0], value[1] + this._player.ga[1]] : [value[0] + this._player.ga[0]];
	}

	get gd() {
		return this.gf - this.ga;
	}

	get score() {
		return (this._player.win * winScore) + (this._player.tie * tieScore);
	}

	get gameId() {
		return this._player.gameId;
	}

	get gamePlayedCount() {
		return this._player.win + this._player.tie + this._player.loose;
	}

	get tBody() {
		return `<td>${this.name}</td>
			<td>${this.gamePlayedCount}</td>
			<td>${this.win}</td>
			<td>${this.tie}</td>
			<td>${this.loose}</td>
			<td>${this.gf}</td>
			<td>${this.ga}</td>
			<td>${this.gd}</td>
			<td>${this.score}</td>`;
	}

	get list() {
		return `<li class="person ${this.name}">
					<p>${this.name}</p>
					<span class="goals">${this.gf}</span>
					${this.score > 0 ? '<span class="winner">winner</span>' : ''}
				</li>`;
	}

	static getDefaultPlayer() {
		return {
			id: 0,
			name: '',
			win: 0,
			tie: 0,
			loose: 0,
			gf: [0,0],
			ga: [0,0]
		}
	}
}