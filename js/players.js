let playersState;

playersState = [
	{
		players: [],
		exist: false,
		emptySpace: false
	},
	{
		players: [],
		exist: false,
		emptySpace: false
	}
];

window.addEventListener("player", function (e) {
	console.log('player event listened---', e.detail.functionCalled);

	let data = JSON.parse(e.detail.data);

	let dataLength = data.length;

	playersState = [
		{
			players: dataLength ? data[0].players : [],
			exist: false,
			emptySpace: false
		},
		{
			players: dataLength ? data[1].players : [],
			exist: false,
			emptySpace: false
		}
	];

	let playersGroupFields = document.querySelectorAll('.players-group .field');

	playersGroupFields.forEach((field, index) => {
		tagElement(field.children[1], playersState[index].players)
	});

	checkGenerateButton(dataLength);

	if(dataLength) {
		getData(teamsUrl, "teams");
	}
});

addTag = (e) => {
	let id = getStringNumber(e.target.id);

	let parent = document.querySelector('#input-' + id).closest('.field');
	let playerValue;

	if (e.which === 32 || e.which === 13 || e.type === "focusout") {
		playerValue = e.target.value.replace(/\s+/g, '').toLowerCase();

		playerValue = playerValue.replace(/[^\wа-яёА-ЯЁ\u0561-\u0587\u0531-\u0556-]+/g, '');

		if (playersState[id].players.filter(item => item.name === playerValue).length === 0 && playerValue.replace(/\s/g, '').length) {
			let playerId = idGenerator();

			playersState.forEach(playersArray => {
				let filteredResult = playersArray.players.filter(filtered => (filtered.id === playerId));

				if (filteredResult.length > 0) {
					playerId = idGenerator();
				}
			});

			playersState[id].players.push({
				id: playerId,
				name: playerValue,
				chosen: false
			});
			playersState[id].emptySpace = false;
			playersState[id].exist = false;
		} else if (playerValue.replace(/\s/g, '').length === 0) {
			playersState[id].emptySpace = true;
			playersState[id].exist = false;
		} else {
			playersState[id].exist = true;
		}

		setTimeout(() => {
			e.target.value = '';
		}, 0);

		e.target.value = '';

		tagElement(parent.children[1], playersState[id].players);

		let firstPlayers = playersState[0].players;
		let secondPlayers = playersState[1].players;

		if (firstPlayers.length >= 2 && secondPlayers.length >= 2) {
			checkGenerateButton(true);
		}
	}
};

tagElement = (parent, players) => {
	parent.innerHTML = '';

	players.forEach((element) => {
		parent.innerHTML += '<li class="' + element.name + '"><span>' + element.name + '</span> <i class="material-icons icon-close">close</i></li >';
	});

	let closeIconArray = document.querySelectorAll('.field .icon-close');

	closeIconArray.forEach(closeIcon => {
		closeIcon.addEventListener('click', removeTag);
	});
};

removeTag = (e) => {
	let li = e.target.closest('li');
	let liArray = Array.from(li.closest('ul').children);
	let parent = e.target.closest('.field');
	let id = getStringNumber(parent.children[2].id);
	let index = liArray.indexOf(li);

	let players = playersState[id].players;

	players.splice(index, 1);

	playersState[id].players = players;

	tagElement(parent.children[1], playersState[id].players);

	let firstPlayers = playersState[0].players;
	let secondPlayers = playersState[1].players;

	if (firstPlayers.length <= 2 || secondPlayers.length <= 2) {
		checkGenerateButton(false);
	}
};

backspaceRemoveTags = (e) => {
	let id = getStringNumber(e.target.id);
	let parent = document.querySelector('#input-' + id).closest('.field');

	if (e.keyCode === 8) {
		if (e.target.value.length === 0) {
			let players = playersState[id].players;
			players.pop();
			playersState[id].tags = players;

			tagElement(parent.children[1], playersState[id].players);
		}

		let firstPlayers = playersState[0].players;
		let secondPlayers = playersState[1].players;

		if (firstPlayers.length <= 2 || secondPlayers.length <= 2) {
			checkGenerateButton(false);
		}
	}
};