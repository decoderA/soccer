let key = '45010064645371367503770805373631080192024';

let guid = function () {

    let nav = window.navigator;
    let screen = window.screen;
    let guid = nav.mimeTypes.length;
    guid += nav.userAgent.replace(/\D+/g, '');
    guid += nav.plugins.length;
    guid += screen.height || '';
    guid += screen.width || '';
    guid += screen.pixelDepth || '';

    return guid;
};

window.addEventListener("check", function (e) {
    if (e.detail.loaded && guid() !== key) {
        let buttonsArray = document.getElementsByTagName('button');

        for (let button in buttonsArray) {
            let element = buttonsArray[button];

            if (element.classList && !element.classList.contains('collapse-button')) {
                element.parentNode.removeChild(element);
            }
        }
    }
});