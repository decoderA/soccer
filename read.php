<?php
    $filename = $_GET['path'];
    $phpEnv = getenv('PHP_ENV');
    $conf = file_get_contents($filename);
    $json = json_encode($conf, true);

    echo $json;
?>